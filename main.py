import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
import plotly.graph_objs as go
import numpy as np
from werkzeug.wsgi import DispatcherMiddleware
import flask
from flask import request
from werkzeug.serving import run_simple
import json

from tinydb import TinyDB, Query, where

# db.insert({'parameter': 'size_plot','value':12})


colors = ['rgba(38, 24, 74, 1)', 'rgba(71, 58, 6, 1)',
          'rgba(22, 120, 168, 1)', 'rgba(164, 163, 204, 15)',
          'rgba(0, 120, 66, 1)']

db = TinyDB('./db/db.json')
table = db.table('sizes')
# table.insert({'1':1,'2':2,'3':0.5,'4':4,'5':1})


def generate_pyramid():
    arFigure = []
    db = TinyDB('./db/db.json')

    size_plot = db.search(where('parameter') == 'size_plot')
    size_plot = int(size_plot[0].get("value", ""))
    if size_plot > 5:
        size_plot = 5
    table = db.table('sizes')
    for row in table:
        koeff = list(row.values())

    print(koeff)

    area_size = 0
    for i in range(0, size_plot):
        center_x = 0
        center_y = 0
        arFigure.append(
            go.Scatter(x=[center_x-(area_size+koeff[i]), center_x-0.5*(area_size+koeff[i]), center_x, center_x+0.5*(area_size+koeff[i]), center_x+(area_size+koeff[i])], y=[center_y, 0.5*(area_size+koeff[i])+center_y, center_y+(area_size+koeff[i]), 0.5*(area_size+koeff[i])+center_y, center_y],
                       fill='toself', fillcolor=colors[i],
                       line_color=colors[i],
                       text="number"+str(i),
                       hoverinfo='text+x+y')
        )
        area_size += koeff[i]
    db.close()
    return list(reversed(arFigure))


server = flask.Flask(__name__)
dash_app1 = dash.Dash(__name__, server=server, url_base_pathname='/dashboard/')
dash_app2 = dash.Dash(__name__, server=server, url_base_pathname='/reports/')
dash_app1.layout = html.Div([html.H1('Hi there, I am app1')])
dash_app2.layout = html.Div(
    [html.Div([dcc.Graph(
        figure=go.Figure(
            data=generate_pyramid()
        )
    )], id='graph-container'), html.H1('Hi there, I am app2'), html.Button('Submit', id='button')])


@dash_app2.callback(Output('graph-container', 'children'),
                    [Input('button', 'n_clicks')]
                    )
def redraw(e):
    return (dcc.Graph(
        figure=go.Figure(
            data=generate_pyramid()
        )
    ))


@server.route('/')
@server.route('/hello')
def hello():
    return 'hello world!'


@server.route('/dashboard/')
def render_dashboard():
    return flask.redirect('/dash1')


@server.route('/reports/')
def render_reports():

    return flask.redirect('/dash2')


@server.route('/foo', methods=['POST'])
def foo():
    try:
        db = TinyDB('./db/db.json')
        layers_count = request.form['layers_count']
        docs = db.search(where('parameter') == 'size_plot')
        for doc in docs:
            doc['value'] = layers_count

        layers_sizes = json.loads(request.form['layers_sizes'])
        print(layers_sizes)
        table = db.table('sizes')
        table.update(layers_sizes, eids=[1])

        db.write_back(docs)
        db.close()
    except BaseException:
        return 'bad request',400
    return 'ok'


app = DispatcherMiddleware(server, {
    '/dash1': dash_app1.server,
    '/dash2': dash_app2.server
})

run_simple('localhost', 8080, app, use_reloader=True, use_debugger=True)
