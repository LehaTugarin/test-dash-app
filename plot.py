import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import numpy as np


df = pd.read_csv(
    'https://gist.githubusercontent.com/chriddyp/'
    'c78bf172206ce24f77d6363a2d754b59/raw/'
    'c353e8ef842413cae56ae3920b8fd78468aa4cb2/'
    'usa-agricultural-exports-2011.csv')

colors = ['rgba(38, 24, 74, 0.8)', 'rgba(71, 58, 6, 0.8)',
          'rgba(22, 120, 168, 0.8)', 'rgba(164, 163, 204, 0.85)',
          'rgba(0, 120, 66, 0.8)']


def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )


def generate_pyramid():
    arFigure = []

    for i in range(1, 5):
        center_x = 0
        center_y = 0
        arFigure.append(
            go.Scatter(x=[center_x-i, center_x-0.5*i, center_x, center_x+0.5*i, center_x+i], y=[center_y, 0.5*i+center_y, center_y+i, 0.5*i+center_y, center_y],
                       fill='toself', fillcolor=colors[i],
                       line_color=colors[i],
                       text="Points + Fills",
                       hoverinfo='text+x+y')
        )
    return list(reversed(arFigure))


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1(children='Test task'),

    html.Div(
        [html.Div(className='card', children=[generate_table(df[['state', 'pork']])], style={'margin': 25, 'padding': 30, 'boxShadow': '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)'}),
         html.Div(className='card', children=[generate_table(df[['state', 'pork', 'poultry', 'dairy', 'fruits proc']])], style={
                  'margin': 25, 'padding': 30, 'boxShadow': '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)'}),

         dcc.Graph(
            figure=go.Figure(
                data=[go.Scatter(x=np.arange(10), y=df['pork'], mode='lines+markers'),
                      go.Scatter(x=np.arange(10),
                                 y=df['poultry'], mode='lines+markers'),
                      go.Scatter(x=np.arange(10),
                                 y=df['dairy'], mode='lines+markers'),
                      go.Scatter(x=np.arange(10),
                                 y=df['fruits fresh'], mode='lines+markers')
                      ],
                layout=go.Layout(
                    title='Simple Scatter',
                    showlegend=True,
                    legend=go.layout.Legend(
                        x=0,
                        y=1.0
                    ),
                    margin=go.layout.Margin(l=40, r=0, t=40, b=30)
                )
            )
        )
        ],

        style={'display': 'flex', 'flexDirection': 'row', 'justifyContent': 'center', 'margin': 40}),

    html.Div(
        [
            dcc.Graph(
                figure=go.Figure(
                    data=generate_pyramid()
                )
            ),
            dcc.Graph(
                figure=go.Figure(
                    data=[go.Scatter(x=np.arange(10), y=df['pork'], mode='lines+markers'),
                          go.Scatter(x=np.arange(10),
                                     y=df['poultry'], mode='lines+markers'),
                          go.Scatter(x=np.arange(10),
                                     y=df['dairy'], mode='lines+markers'),
                          go.Scatter(x=np.arange(10),
                                     y=df['fruits fresh'], mode='lines+markers')
                          ],
                    layout=go.Layout(
                        title='Simple Scatter',
                        showlegend=True,
                        legend=go.layout.Legend(
                            x=0,
                            y=1.0
                        ),
                        margin=go.layout.Margin(l=40, r=0, t=40, b=30)
                    )
                )
            )
        ],

        style={'display': 'flex', 'flexDirection': 'row', 'justifyContent': 'center', 'margin': 40}),

])

if __name__ == '__main__':
    app.run_server(debug=True)
